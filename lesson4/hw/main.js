import '@babel/polyfill';

import * as ArticlesModel from './articles';

async function onLoad(){
    let articles = await ArticlesModel.all();
    console.log('articles count = ' + articles.length);

    let ind = Math.floor(Math.random() * articles.length);
    console.log('select index ' + ind + ', id = ' + articles[ind].id);
    let article = await ArticlesModel.one(articles[ind].id);

    console.log(article);
    let res = await ArticlesModel.remove(article.id);

    console.log('что с удалением? - ' + res);
    let newArticles = await ArticlesModel.all();
    console.log('articles count = ' + newArticles.length);
}

onLoad().then().catch((e) => {
    console.log(e);
});

/*
ArticlesModel.all()
    .then((articles) => {
        console.log('articles count = ' + articles.length);
        let ind = Math.floor(Math.random() * articles.length);
        console.log('select index ' + ind + ', id = ' + articles[ind].id)

        return ArticlesModel.one(articles[ind].id);
    })
    .then((article) => {
        console.log(article);
        return ArticlesModel.remove(article.id);
    })
    .then((res) => {
        console.log('что с удалением? - ' + res);
        return ArticlesModel.all()
    })
    .then((articles) => {
        console.log('articles count = ' + articles.length);
    })
    .catch((e) => {
        console.log(e);
    });
*/